<?php

namespace PMC_FixWT;

use pocketmine\plugin\PluginBase;

class PMC_FixWT extends PluginBase {

	public function onLoad(){
		@mkdir($this->getDataFolder());
		$this->saveDefaultConfig();
	}

	public function onEnable(){
		$cfg = $this->getConfig()->getAll();
		$level = $this->getServer()->getDefaultLevel();

		$worldName = $level->getName();
		if(!$this->getServer()->isLevelLoaded($worldName)){
			$this->getServer()->loadLevel($worldName);
		}

		//SET Weather
		if($cfg['fixWeather'] && method_exists($level, 'getWeather')){
			$weather = $level->getWeather();
			$weatherID = $cfg['weather'];
			switch($weatherID){
				case $weather::SUNNY:
					$weatherName = "'Солнечно'";
					break;
				case $weather::RAINY:
					$weatherName = "'Дождь'";
					break;
				case $weather::RAINY_THUNDER:
					$weatherName = "'Шторм'";
					break;
				case $weather::THUNDER:
					$weatherName = "'Снег'";
					break;
				default:
					$weatherName = "'Солнечно'";
					$weatherID = 0;
			}
			$weather->setWeather($weatherID, 12000 * 100); //0 - SUNNY, в течение 100 дней
			$weather->setCanCalculate(false);
			$this->getLogger()->info("§6Погода установлена в " . $weatherName);
		}

		if($cfg['fixTime']){
			$mcTime = intval($cfg['time']);
			$level->setTime($mcTime);
			$level->stopTime();
			$realTime = $mcTime / 1000 + 6;
			$hh = $realTime % 24;
			$drob = $realTime - floor($realTime);
			$mm = floor(60 * $drob);
			$this->getLogger()->info("§6Игровое время суток зафиксировано на $hh ч $mm мин!");
		}
	}
}
